
# this is mostly here to indicate that this
# script requires sudo
sudo touch LAST_UPDATED

# get pip
sudo apt-get install python-pip libssl-dev libpcre3 libpcre3-dev

# make sure pip is up to date
sudo pip install pip -U
sudo pip install "virtualenv>=1.10"

if [ ! -e ascii-mmorpg-linux ]; then
    virtualenv ascii-mmorpg-linux
fi

# local aliases for easy use
if [ ! -e bin ]; then
    ln -s ascii-mmorpg-linux/bin 
fi

# adding bin to make installs in the virtual env
bin/pip install -r requirements.txt
