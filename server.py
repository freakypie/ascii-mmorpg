from collections import OrderedDict

from django.utils import autoreload

from geventwebsocket.resource import Resource
from geventwebsocket.server import WebSocketServer
from mmorpg.wsgi import AsciiApplication, django_app


if __name__ == "__main__":


    def serve():

        print "starting application..."

        WebSocketServer(
            ('', 8000),
            Resource(OrderedDict((
                ("/-/server/", AsciiApplication),
                ("/", django_app),
            ))),
        ).serve_forever()

    autoreload.main(serve)

