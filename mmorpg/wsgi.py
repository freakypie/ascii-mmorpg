from os import path
import os
import sys
import uuid

from django.core.wsgi import get_wsgi_application
from geventwebsocket.exceptions import WebSocketError

from geventwebsocket.resource import WebSocketApplication
from mmorpg.server import Server
import json
import gevent


path = path.abspath("..")
sys.path.insert(0, path)
sys.path.insert(0, os.getcwd())

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mmorpg.settings.local")
django_app = get_wsgi_application()


class AsciiApplication(WebSocketApplication):
    server = None
    sprite = None
    dirty = False

    def send(self, packet):
        self.ws.send(json.dumps(packet))

    def on_open(self):
        if not AsciiApplication.server:
            AsciiApplication.server = Server()
            gevent.spawn_later(0, AsciiApplication.server.main)
            print "started server"

        self.id = str(uuid.uuid4())

        self.server.add_client(self)

    def on_message(self, packet):
        if packet:
            self.server.message(self, json.loads(packet))

    def on_close(self, reason):
        print reason

