import random
import math



game_map = """
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX====XX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX====XX
XJ  t   t         xx   xx         XX
XJ    t           xx   xx-------- XX
XJ   t       T    xx              XX
XJ             T  xx------------- XX
XJ                  =      =      XX
XJ                  =T____T=      XX===XX
XJ                  =T____Y=      xx---XX
XJ   T              =Y____T=           XX
XJ  T T  T          =      =  XX=======XX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=======XX
"""


class Node(object):
    type = "empty"


class Map(object):
    spawns = []

    def __init__(self, map_string):
        self.map = map_string.strip()
        self.nodes = []

        y = 0
        x = 0
        for item in self.map:
            data = {
                "character": item,
                "type": "empty",
                "x": x,
                "z": y,
                "solid": True,
                "angle": dict(x=-math.pi / 2, y=0, z=0)
            }

            if item == "\n":
                y += 1
                x = 0
                continue

            elif item == "t":
                data["type"] = "tree"
                data["color"] = "green"
#                 data["scale"] = random.random() * 2 + 2.0
                data["angle"] = dict(x=0, y=random.random() * 1 - 0.5, z=0)

            elif item in "TY":
                data["type"] = "tree"
                data["color"] = "green"
                data["scale"] = random.random() * .2 + 2.0
                data["angle"] = dict(x=0, y=random.random() * 1 - 0.5, z=0)

            elif item == "_":
                data["type"] = "spawn"
                data["solid"] = False

            elif item in "=-":
                data["type"] = "road"
                data['solid'] = False

            elif item in "xX":
                data["type"] = "wall"
                data["scale"] = 1
                data["color"] = "black"
                data["background"] = "rgba(0, 0, 0, 0.75)"

            if item != " ":
                self.nodes.append(data)

            x += 1

    def filter(self, ntype=None):
        retval = []
        for node in self.nodes:
            if ntype == node["type"]:
                retval.append(node)
        return retval
