import random
import time

import gevent
from geventwebsocket.exceptions import WebSocketError
from geventwebsocket.handler import WebSocketHandler
from mmorpg.utils import find_nth
from mmorpg.map import Map, game_map


class Server(object):
    clients = {}
    frame = 0
    interval = 1 / 15.0
    dirty = False

    def __init__(self):
        self.frame = 0
        self.map = Map(game_map)

    def main(self):
        self.last = time.time()
        while True:
            gevent.sleep(self.interval)
            assert self.last + self.interval <= time.time()
            self.last = time.time()

            if self.dirty:
                self.dirty = False
                self.frame += 1

                players = {}
                for cid, client in self.clients.items():
                    if client.sprite and client.dirty:
                        client.dirty = False
                        print "adding update", cid
                        players[cid] = client.sprite

                        if "remove" in client.sprite:
                            del self.clients[cid]

                self.send_all({
                    "event": "update",
                    "data": {
                        "frame": self.frame,
                        "players": players
                    }
                })

#     def run(self):
#         print "running server"
#
#         while True:
#             start = time.time()
#             while start < next_start:
#                 start = time.time()
#
#             self.main()
#             next_start += self.interval

    def send_all(self, packet):

        for wid, client in self.get_clients().items():
            try:
#                 print "sending", packet, "to", wid
                client.send(packet)
            except WebSocketError:
                print "client died"
                self.remove_client(client)

    def get_clients(self):
        return self.clients

    def add_client(self, client):
        self.clients[client.id] = client

        # tell them who they are
        client.send({
            "event": "welcome",
            "data": {
                "id": client.id,
                "frame": self.frame,
                "map": self.map.nodes
            }
        })

    def remove_client(self, client):
        if client.sprite:
            self.clients[client.id].sprite['remove'] = True
            self.clients[client.id].dirty = True
        else:
            del self.clients[client.id]

    def message(self, client, packet):
        event = "on_" + packet["event"]
        attr = getattr(self, event, None)
        if attr:
            attr(client, packet['data'])

    def on_spawn(self, client, data):
        data["id"] = client.id
        node = random.choice(self.map.filter("spawn"))
        data["x"] = node["x"]
        data["z"] = node["z"]

        # TODO: check if there is already a unit

        client.sprite = data
        self.clients[client.id].dirty = True
        self.dirty = "spawn"

        print "spawned", client.id, self.dirty
        print hash(self)

    def on_update(self, client, data):
        client.sprite.update(data)
        self.dirty = "update: %s" % client.id
        self.clients[client.id].dirty = True


if __name__ == "__main__":
    server = Server()
    server.run()
