
function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
             .toString(16)
             .substring(1);
};

function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}

function unproject(screen, event) {
    var pos = event;
    if (event.originalEvent.touches) {
        pos = event.originalEvent.touches[0];
    }    
    var y = pos.pageY - $(screen.renderer.domElement).offset().top;
    var x = pos.pageX - $(screen.renderer.domElement).offset().left;    
    var w = $(screen.renderer.domElement).width();
    var h = $(screen.renderer.domElement).height();
    
    // must not use canvas width and height because canvas reports actual 
    // pixels and we need relative pixels (retina screens ruin stuff)
    
    var vector = new THREE.Vector3((x / w) * 2 - 1, 0, (y / h) * -2 + 1).normalize(); 
//    vector.x = -vector.x;
    vector.z = -vector.z;
//    screen.projector.unprojectVector(vector, screen.camera);   
//
//    var raycaster = new THREE.Raycaster( screen.camera.position, 
//        vector.sub( screen.camera.position ).normalize() );
//
//    var intersects = raycaster.intersectObjects( screen.scene.children );
//
//    if ( intersects.length > 0 ) {
//        console.log("intersects");
//        vector = intersects[ 0 ].point;
//
        $("#cursor").text("x: " + x + " y: " + y + 
            " (" + vector.x.toFixed(3) + ", " + vector.y.toFixed(3) + ", " + vector.z.toFixed(3) + ")");
//        
//        return new THREE.Vector3(vector.x, 0, vector.z);
//    }
        return vector.multiplyScalar(10.0);
}
