

function Screen(options) {
    this.constructor(options);
}

Screen.prototype = {
    constructor: function (options) {
        this.options = options;  
        this.blockSize = options.blockSize || 25.0;

        var el = $("body")
        var w = window.innerWidth;
        var h = window.innerHeight;
        
        this.camera = new THREE.PerspectiveCamera(40, w / h, 1, 10000);
        this.camera.position.z = 400;
        this.camera.position.x = 400;
        this.camera.position.y = 400;
        
        this.camera.rotation.x = -1.0;
        
        this.scene = new THREE.Scene();   
        this.renderer = new THREE.CSS3DRenderer();
        this.projector = new THREE.Projector();
        
        this.renderer.setSize(w, h);
        this.renderer.domElement.style.position = 'absolute';
        this.renderer.domElement.style.top = '0';
        this.renderer.domElement.style.left = '0';
        el.append(this.renderer.domElement);        
    },
    addNode: function(node) {
        var me = this;
        var span = $('<span class="char">').html(node.character);
        
        var size = this.blockSize * (node.scale || 1); 
        span.css({
            "font-size": size + "px",
            color: node.color || "black",
            width: size + "px",
            height: size + "px",
            background: node.background || "inherit"
        });        

        var object = new THREE.CSS3DObject(span.get(0));
        span.data("object", object);
        object.position.x = node.x * this.blockSize;
        object.position.y = (node.type) ? size / 2 : 0;
        object.position.z = node.z * this.blockSize;
        if (node.angle) {
            object.rotation.x = node.angle.x || 0;
            object.rotation.y = node.angle.y || 0;
            object.rotation.z = node.angle.z || 0;
        }
        this.scene.add(object);
        
        return object;
    },
    loadMap: function (map) {
        for (var c in this.map) {
            $(this.map[c].object.element).remove();
        }        
        this.scene.children.splice(0, this.scene.children.length);

        this.map = map;
        for (var key in map) {
            this.map[key].object = this.addNode(map[key]);
        }
        console.log("added nodes");
        //this.refresh();
    },
    getMapNode: function(vector) {
        var x = Math.floor(vector.x / this.blockSize + 0.5);
        var z = Math.floor(vector.z / this.blockSize + 1.0);
        
        for (key in this.map) {
            if (this.map[key].x == x && this.map[key].z == z) {
                return this.map[key];
            }
        }        
        return {character: " ", solid: false};
    }
}


