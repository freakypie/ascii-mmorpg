

var attempts = 0;
var maxAttempts = 5;
var s = null;
var my_screen = null;
var player = null;
var frame = 0;
var players = {
    
};

function connect() {
	console.log("connecting");
	
	s = new WebSocket("ws://www.leithall.com:8000/-/server/");
	s.onopen = function() {
		console.log("connected !!!");
		attempts = 0;
	};
	s.onmessage = function(e) {
	    if (e.data instanceof Blob) {
	        return 
	    }
	    
	    var packet = JSON.parse(e.data);
//        console.log("message", packet);
        
        switch(packet.event) {
        case "welcome":
            s.id = packet.data.id
            my_screen.loadMap(packet.data.map);
            frame = packet.data.frame;
            
            var chars = ["&#x2625;"]
            send({
                "event": "spawn",
                "data": {
                    "scale": 2,
                    "character": chars[Math.floor(Math.random() * chars.length)] 
                }
            });
            
            break;
            
        case "update":
            if (packet.data.frame > frame) {
                frame = packet.data.frame;
                                
                var update = packet.data.players;
                for (var key in update) {
                    if (key != s.id) {
                        console.log("updates from", key);
                        if (! players[key]) {
                            players[key] = new Sprite({
                                object: my_screen.addNode(update[key]),
                                behaviors: [new SwordWielding({screen: my_screen})]
                            });
                            players[key].id = key;
                        } else {
                            players[key].object.position = new THREE.Vector3(
                                update[key].x, 
                                0,                                
                                update[key].z);
                            
                            players[key].life = update[key].life;
                            
                            if (update[key].mouse) {
                                players[key].mouse = new THREE.Vector3(
                                    update[key].mouse.x, 
                                    0,                                
                                    update[key].mouse.z
                                );
                            } else {
                                players[key].mouse = null;
                            }
                        }
                    } else if (! player) {
                        console.log("spawning unit", update[key]);
                        player = new Sprite({
                            object: my_screen.addNode(update[key]),
                            behaviors: [new PlayerControls({screen: my_screen}),
                                        new SwordWielding({screen: my_screen})]
                        });
                        player.id = s.id;
                        console.log(player);
                        $(player.object.element).css("-webkit-transition", "none");
                        players[s.id] = player;
                    }                    
                }            
                
            } else {
                console.warn("dumped old packet");
            }
            break;
        }
	};
	s.onerror = function(e) {
		console.error("error",e);
	};
	s.onclose = function(e) {
		console.log("connection closed");		
		if (attempts < maxAttempts) {
			attempts += 1;
			console.log("retrying connection", attempts);
			setTimeout(connect, 1000 * attempts);
		} else {
			console.warn("Not trying to connect again")
		}
	};
}

function send(packet) {
	if (s) {
		s.send(JSON.stringify(packet));
	}
}


function Game(options) {
    this.constructor(options);
}

Game.prototype = {
    constructor: function (options) {
        this.sprites = players;
        this.screen = my_screen;
    },
    execute: function() {  
        for (var cid in this.sprites) {
            if (! this.sprites[cid].execute()) {
                this.sprites[cid].remove();
            }
        }       
        
        if (player && player.dirty) {
            player.dirty = false;
            send({
                event: "update",
                data: {
                    life: player.life,
                    x: player.object.position.x,
                    z: player.object.position.z,
                    mouse: player.mouse ? {
                        x: player.mouse.x,
                        z: player.mouse.z
                    } : null
                }            
            });
        }
    },
    addSprite: function (sprite) {
        if (! sprite.id) {
            sprite.id = guid();
        }
        this.sprites[sprite.id] = sprite;
    }
};

$(function() {
	connect();
	my_screen = new Screen({element: "#blackboard"});
	game = new Game();	
	setInterval(game.execute.bind(game), 1000/15);
	    
	var frameStart = new Date().getTime();
	var frameCount = 0;
	
    function render() {
        window.requestAnimationFrame(render);

        frameCount += 1;
        if (frameCount / 200 == Math.floor(frameCount / 200)) {
            console.log("fps", frameCount * 1000 / (new Date().getTime() - frameStart));
        }
        
        my_screen.renderer.render(my_screen.scene, my_screen.camera);  
    }
    
    render();
});

$(window).on("beforeunload", function() {
	attempts = 1000;
	s.close();
});