

function Sprite(options) {
    this.constructor(options);
}

Sprite.prototype = {
    constructor: function(options) {
        this.object = options.object;
        this.life = 100;
        this.behaviors = options.behaviors || [];
    },
    execute: function() {
        for (var x in this.behaviors) {
            this.behaviors[x].execute(this);
        }
                
        return this.life > 0;
    },
    remove: function () {
        game.screen.scene.remove(this);
        $(this.object.element).hide().remove();
        delete game.sprites[this.id];
    }
}


function SwordWielding(options) {
    this.constructor(options);
}

SwordWielding.prototype = {
    constructor: function (options) {
        this.options = options;

        // create sword
        this.sword = options.screen.addNode({
            character: "&#x219E;",
            scale: 1.5
        });        
        $(this.sword.element).addClass("sword");
        $(this.sword.element).hide();                
    },
    execute: function (sprite) {
        if (sprite.mouse) {                
            var vec = sprite.object.position.clone().sub(sprite.mouse).normalize();
            var angle = Math.atan2(vec.z, -vec.x)
             
            this.sword.rotation.reorder("YZX");
            this.sword.rotation.y = angle;
            this.sword.rotation.x = Math.PI / 2;
            
            // update sword position
            this.sword.position = sprite.object.position.clone().add(
                new THREE.Vector3(25, -12, 15).applyAxisAngle(
                    new THREE.Vector3(0, 1, 0), this.sword.rotation.y
                )
            );
            
            // check for damage!
            if (this.previous) {
                var attack = sprite.object.position.clone().add(
                    new THREE.Vector3(40, -12, 15).applyAxisAngle(
                        new THREE.Vector3(0, 1, 0), this.sword.rotation.y
                    )
                );
                var dist = 0;
                for (var cid in game.sprites) {
                    dist = attack.distanceTo(game.sprites[cid].object.position);
                    if (dist < 25.0) {
                        if (Math.random() > 0.5) {
                            
                            var scale = Math.floor(
                                this.previous.distanceTo(this.sword.position) + 
                                this.previous.distanceTo(this.sword.position)
                                * Math.random()
                            );

                            if (scale > 0) {
                                var blood = new Sprite({
                                    object: game.screen.addNode({
                                        character: "-" + scale,
                                        color: "red",
                                        scale: 0.7
                                    }),
                                    behaviors: [new ParticleBehavior()]
                                });
                                
                                if (cid == s.id) {
                                    game.sprites[cid].life -= scale;
                                    game.sprites[cid].dirty = true;
                                }
                                
                                game.addSprite(blood);
                                blood.object.position = game.sprites[cid].object.position.clone();
                            }
                        }
                    }
                }
            }
            
            this.previous = this.sword.position;
            
            $(this.sword.element).show();                
        } else {
            $(this.sword.element).hide();
            this.previous = null;
        }
        
        if (sprite.life <= 0) {
            game.screen.scene.remove(this.sword);
            $(this.sword.element).remove().hide();
        }
    }
}


function ParticleBehavior(options) {
    this.constructor(options);
}

ParticleBehavior.prototype = {
    constructor: function () {
        
    },
    execute: function (sprite) {
        sprite.life -= 10;
        var scale = 15.0;
        sprite.object.position.x += Math.random() * scale - scale / 2;
        sprite.object.position.y += Math.random() * scale;
        sprite.object.position.z += Math.random() * scale - scale / 2;
    }
}


function PlayerControls(options) {
    this.constructor(options);
}

PlayerControls.prototype = {
    constructor: function(options) {
        this.screen = options.screen;
        this.speed = options.speed || 4.0;
        this.element = this.screen.renderer.domElement;
        
        var me = this;
        $(window).keydown(function(e) {
            me.key = e.which;
        });
        $(window).keyup(function(e) {
            me.key = e.which;
            me.keyup = true;
        });
        $(window).on("mousedown", function(e) {
            me.mouse = unproject(me.screen, e);
        });
        $(window).on("mousemove", function(e) {
            if (me.mouse) {
                me.mouse = unproject(me.screen, e);            
            }
        });
        $(window).on("mouseup", function(e) {
            me.mouse = false;
        });
    },   
    execute: function(sprite) {

        // watch for action keys
        if (this.key) {
            
            var delta = null;
            
            if (this.key == 65 || this.key == 37) {
                delta = new THREE.Vector3(-this.speed, 0, 0);
            }
            if (this.key == 68 || this.key == 39) {
                delta = new THREE.Vector3(this.speed, 0, 0);
            }
            if (this.key == 87 || this.key == 38) {
                delta = new THREE.Vector3(0, 0, -this.speed);
            }
            if (this.key == 83 || this.key == 40) {
                delta = new THREE.Vector3(0, 0, this.speed);
            }
                            
            if (delta !== null) {
                var pos = sprite.object.position.clone().add(delta);
                if (! this.screen.getMapNode(pos).solid) {
                    sprite.object.position.add(delta);
                    sprite.dirty = true;
                }
            }

            // if up, clear it
            if (this.keyup) {
                this.keyup = false;
                this.key = null;
            }
        }
        
        if (this.mouse) {
            sprite.mouse = sprite.object.position.clone().add(this.mouse);
            sprite.dirty = true;
        } else {
            if (sprite.mouse !== null) {
                sprite.dirty = true;
            }
            sprite.mouse = null;
        }
        
        
        // follow player
        var y = this.screen.camera.position.y;
        var pos = sprite.object.position.clone();
        pos.z += y * 0.7;
        this.screen.camera.position = this.screen.camera.position.lerp(pos, .1);
        this.screen.camera.position.y = y;
    }
}