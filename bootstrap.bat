REM requires git command line tools
REM install git for windows and check the option to install 
REM linux tools directly on the path

@ECHO OFF

REM this is mostly here to indicate that this
REM script requires sudo

touch LAST_UPDATED

REM make sure pip is up to date

pip install pip -U
pip install "virtualenv>=1.10"

IF NOT EXIST ascii-mmorpg-windows (
    virtualenv ascii-mmorpg-windows
)


REM local aliases for easy use

IF NOT EXIST bin (
    ln -s ascii-mmorpg-windows/scripts bin
)

REM adding bin to make installs in the virtual env

bin\pip install -r requirements.txt
